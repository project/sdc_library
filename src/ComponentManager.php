<?php

namespace Drupal\sdc_library;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Asset\AssetQueryString;
use Drupal\Core\Plugin\Component;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ComponentPluginManager;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Url;
use Drupal\sdc\Exception\ComponentNotFoundException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Component manager service.
 */
class ComponentManager implements ComponentManagerInterface {

  // This gives access to $this->t().
  use StringTranslationTrait;

  /**
   * A lookup map of property type (string, number etc) to fallback defaults.
   */
  final public const PROPERTY_TYPE_DEFAULTS = [
    'string' => 'This is fallback text defined in the SDC Library module. Please add defaults to your component so that we can use them to render the component.',
    'integer' => 3,
    'number' => 3.14,
    'boolean' => 1,
    'null' => NULL,
    'array' => [
      'foo',
      'bar',
      'baz',
    ],
    // @todo Not sure how to handle objects, at this point.
    'object' => '',
  ];

  /**
   * Constructor.
   */
  public function __construct(
    protected RequestStack $requestStack,
    protected $formBuilder,
    private ComponentPluginManager $pluginManager,
    protected ThemeManagerInterface $themeManager,
    public string $sdcWelcomeComponent,
    public string $sdcIframeWrapperId,
    public string $sdcRenderedComponentWrapperId,
    protected AssetQueryString $assetQueryString,
    protected TimeInterface $time,
    protected StateInterface $state,
    protected $renderer,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('request_stack'),
      $container->get('form_builder'),
      $container->get('plugin.manager.sdc'),
      $container->get('theme.manager'),
      $development_mode = (bool) $container->getParameter('sdc_library.development'),
      $welcome_component = (string) $container->getParameter('sdc_library.welcome'),
      $iframe_wrapper_id = (string) $container->getParameter('sdc_library.iframe_wrapper_id'),
      $rendered_component_wrapper_id = (string) $container->getParameter('sdc_library.rendered_component_wrapper_id'),
      $asset_query_string = $container->get('@asset.query_string'),
      $time = $container->get('datetime.time'),
      $state = $container->get('state'),
      $container->get('renderer'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getComponents(): ?array {
    $request = $this->requestStack->getCurrentRequest();
    $components = $this->pluginManager->getAllComponents();
    $items = [];
    foreach ($components as $component) {
      $route_parameters = [
        'component_plugin' => $component->getPluginId(),
        'chosen_theme' => $request->get('chosen_theme') ?? $this->themeManager->getActiveTheme()->getName(),
      ];
      $url = Url::fromRoute('sdc_library.browse', $route_parameters);
      $items[] = [
        'url' => $url,
        'href' => $url->toString(),
        'name' => $component->metadata->name,
        'status' => $component->metadata->status,
        'group' => $component->metadata->group,
        'extension' => $this->findExtensionName($component->metadata->path),
      ];
    }

    // Call sorting method, found in this class.
    usort($items, [$this, 'sortByComponentName']);
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function getComponentsTree(): ?array {
    $request = $this->requestStack->getCurrentRequest();
    $components = $this->pluginManager->getAllComponents();
    // Instantiate variable to hold site components, grouped by theme/module.
    $items = [];
    foreach ($components as $component) {
      $definition = $component->getPluginDefinition();
      $group_key_name = strtolower("{$definition['extension_type']->name}:{$definition['provider']}");
      // @todo Not the slickest, but all we do is add a space between the "name"
      // and the "provider" values so it's easier for the user to read.
      $group_display_name = "{$definition['extension_type']->name}: {$definition['provider']}";
      if (!isset($items[$group_key_name])) {
        $items[$group_key_name] = [];
        $items[$group_key_name]['id'] = $definition['id'];
        $items[$group_key_name]['title'] = $group_display_name;
        $items[$group_key_name]['name'] = $group_display_name;
      }
      // We need to provide route parameters to build our $url variable.
      $route_parameters = [
        'component_plugin' => $component->getPluginId(),
        'chosen_theme' => $request->get('chosen_theme') ?? $this->themeManager->getActiveTheme()->getName(),
      ];

      $url = Url::fromRoute('sdc_library.browse', $route_parameters);
      // It is in the "below" array that we add this group's components.
      // The "below" key is used in twig to build out menu-like structure.
      $items[$group_key_name]['below'][] = [
        'url' => $url,
        'href' => $url->toString(),
        'title' => $component->metadata->name,
        'name' => $component->metadata->name,
        'status' => $component->metadata->status,
        'group' => $component->metadata->group,
        'extension' => $this->findExtensionName($component->metadata->path),
      ];
      // This sorts the components within their parent groupings. My gut says it
      // is wrong to sort on each loop iteration, but it works!
      usort($items[$group_key_name]['below'], [$this, 'sortByComponentName']);
    }
    // This sorts the parent groupings.
    usort($items, [$this, 'sortByComponentName']);

    // Let's shift the SDC Library components group to the beginning of items.
    $sdc_library_index = array_search('sdc_library:welcome', array_column($items, 'id'));
    // Grab the contents of our SDC Library group so we can unset it and add it
    // back with array_unshift(). There's likely a more eloquent way to do this.
    $temp_array_holder = $items[$sdc_library_index];
    unset($items[$sdc_library_index]);
    array_unshift($items, $temp_array_holder);

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function renderComponentList(): array {
    $items = $this->getComponents();
    $items['tree'] = $this->getComponentsTree();
    // Early return with warning message if we find no components.
    if (!$items) {
      return [
        '#markup' => '<div class="messages messages--warning"><h3>' . $this->t('Unable to find any components') . '</h3>' . $this->t('Check that the modules or themes containing the components are enabled and the components are correctly named.</div>'),
      ];
    }
    $build = [];
    // Grab form.
    $build[] = [
      '#theme' => 'sdc_list',
      '#items' => $items,
      '#component_form' => $this->renderComponentForm(),
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function renderComponentForm(): array {
    return $this->formBuilder->getForm('Drupal\sdc_library\Form\ComponentForm');
  }

  /**
   * {@inheritdoc}
   */
  public function renderComponent(): array {
    $request = $this->requestStack->getCurrentRequest();
    try {
      $build = $this->generateRenderArray(
        $this->getComponentInstance($request->get('component_plugin')),
        $this->getArguments()
      );
    }
    catch (ComponentNotFoundException $e) {
      $build = [
        '#markup' => '<div class="messages messages--error"><h3>' . $this->t('Unable to find that component') . '</h3>' . $this->t('Check that the module or theme containing the component is enabled and the component is correctly named. Message: %message', ['%message' => $e->getMessage()]) . '</div>',
      ];
    }

    // This changes the asset query string so that fresh css/js is loaded.
    $this->assetQueryString->reset();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getComponentInstance(string $sdc_plugin): Component {
    if (!$sdc_plugin) {
      throw new ComponentNotFoundException('We need a component name to find it.');
    }
    // Clearing out the cache to keep things fresh.
    $this->pluginManager->clearCachedDefinitions();
    return $this->pluginManager->createInstance($sdc_plugin);
  }

  /**
   * {@inheritdoc}
   */
  public function getArguments(): array {
    $request = $this->requestStack->getCurrentRequest();
    if ($request->getMethod() === 'GET') {
      $params = [$request->get('component_plugin'), $request->get('chosen_theme')];
    }
    if ($request->getMethod() === 'POST') {
      $params = $request->request->all();
    }
    return $params;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropsFromComponentMetadata(array $schema_properties): ?array {
    $props = [];
    foreach ($schema_properties as $property => $property_data) {
      // The property type is always in an array but we want a scalar value.
      $type = array_shift($property_data['type']);
      if (isset($type)) {
        // This crucial bit is where we determine what to pass as the default.
        if (isset($property_data['default'])) {
          $props[$property] = $this->getPropertyValue($type, $property_data['default']);
        }
        elseif (isset($property_data['examples'])) {
          // Using array_shift to get the first value as there could be many.
          $props[$property] = array_shift($property_data['examples']);
        }
        else {
          // We have no defaults to use so let's point that out.
          $props[$property] = self::PROPERTY_TYPE_DEFAULTS[$type];
        }
      }
    }
    return $props;
  }

  /**
   * {@inheritdoc}
   */
  public function getSlotsFromComponentMetadata(array $metadata_slots): ?array {
    $slots = [];
    foreach ($metadata_slots as $slot => $slot_data) {
      // We assume there are examples, use the first one.
      if (isset($slot_data['examples'][0])) {
        $slots[$slot] = [
          '#type' => 'processed_text',
          '#format' => 'full_html',
          '#text' => $slot_data['examples'][0],
        ];
      }
    }
    return $slots;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropsFromPostedForm(Component $component, array $posted_data): ?array {
    $props = [];
    if (isset($posted_data['props'][$component->getPluginId()])) {
      foreach ($posted_data['props'][$component->getPluginId()] as $property => $property_data) {
        $type = $property_data['type'];
        if (isset($type) && isset($property_data['default'])) {
          $props[$property] = $this->getPropertyValue($type, $property_data['default']);
        }
      }
    }
    return $props;
  }

  /**
   * {@inheritdoc}
   */
  public function getSlotsFromPostedForm(Component $component, array $posted_data): ?array {
    $slots = [];
    if (isset($posted_data['slots'][$component->getPluginId()])) {
      foreach ($posted_data['slots'][$component->getPluginId()] as $slot => $slot_data) {
        // We use array_shift() because the slot may have multiple examples
        // which means the form keys them with 0, 1, 2 etc.
        $slot_data = array_shift($slot_data);
        // Return a render array, otherwise markup gets escaped.
        $slots[$slot] = [
          '#type' => 'processed_text',
          '#format' => $slot_data['format'],
          '#text' => $slot_data['value'],
        ];
      }
    }
    return $slots;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyValue(string $type, mixed $property_value): mixed {
    // The type can be any number of types: string, number, integer, array etc.
    switch ($type) {
      case 'array':
        // The component's array example will likely give us an array for
        // $property_value whereas the submitted form gives a string.
        if (gettype($property_value) == 'string') {
          // We assume if it is a string, then it is json encoded.
          $property_value = json_decode($property_value);
        }
        $default_value = $property_value;
        break;

      case 'boolean':
        $default_value = boolval($property_value);
        break;

      case 'integer':
        $default_value = (int) $property_value;
        break;

      case 'null':
        // Hey, if it's a null type then we can just pass NULL, no?
        $default_value = NULL;
        break;

      case 'number':
        $default_value = (float) $property_value;
        break;

      case 'object':
        // @todo Will the property value come in as an array?
        $default_value = (object) $property_value;
        break;

      case 'string':
        // Create a render array, for the string may contain legitimate markup.
        $render_array = [
          '#type' => 'processed_text',
          '#format' => 'full_html',
          '#text' => $property_value,
        ];
        // WYSIWYG fields definitely have markup and have this array structure.
        if (
          is_array($property_value)
          && isset($property_value['value'])
          && $property_value['format']
        ) {
          $render_array = [
            '#type' => 'processed_text',
            '#format' => $property_value['format'],
            '#text' => $property_value['value'],
          ];
        }
        $default_value = $this->renderer->render($render_array);
        break;

      default:
        $default_value = $property_value;
        break;
    }

    return $default_value;
  }

  /**
   * {@inheritdoc}
   */
  public function generateRenderArray(Component $component, array $context): array {
    /** @var Drupal\sdc\Component\ComponentMetadata $metadata */
    $metadata = $component->metadata;
    // The properties can be built from the SDC's yml data or from POST data.
    // Check which one, get the data we need in a normalized way, and then
    // continue to build the render array.
    if ($this->requestStack->getCurrentRequest()->request->all()) {
      // This is structured as the POSTed form.
      $props = $this->getPropsFromPostedForm($component, $this->requestStack->getCurrentRequest()->request->all());
      $slots = $this->getSlotsFromPostedForm($component, $this->requestStack->getCurrentRequest()->request->all());
    }
    else {
      // A regular page load gives us a PHP array structure of the SDC's yml.
      $props = $this->getPropsFromComponentMetadata($metadata->schema['properties']);
      $slots = $this->getSlotsFromComponentMetadata($metadata->slots);
    }

    $rendered_component = [
      '#type' => 'component',
      '#component' => $component->getPluginId(),
      '#props' => $props,
      '#slots' => $slots,
    ];

    $build = [];
    // Grab form.
    $build[] = [
      '#theme' => 'sdc_component',
      '#item' => $component,
      '#rendered_component' => [
        '#type' => 'processed_text',
        '#format' => 'full_html',
        '#text' => $this->renderer->render($rendered_component),
      ],
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function findExtensionName(string $path): ?string {
    if (empty($path)) {
      return NULL;
    }
    $path = dirname($path);
    $dir = basename($path);
    $info_file = $path . DIRECTORY_SEPARATOR . "$dir.info.yml";
    if (file_exists($info_file)) {
      return $dir;
    }
    return $this->findExtensionName($path);
  }

  /**
   * {@inheritdoc}
   */
  public function sortByComponentName(array $a, array $b): int {
    return strcmp($a['name'], $b['name']);
  }

  /**
   * {@inheritdoc}
   */
  public function findComponentFile(string $filename): ?string {
    if (empty($filename)) {
      return NULL;
    }
    if (file_exists($filename)) {
      return $filename;
    }
    $parts = explode(DIRECTORY_SEPARATOR, $filename);
    array_shift($parts);
    $filename = implode(DIRECTORY_SEPARATOR, $parts);
    return $this->findComponentFile($filename);
  }

}
