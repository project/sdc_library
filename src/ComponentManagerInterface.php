<?php

namespace Drupal\sdc_library;

use Drupal\Core\Plugin\Component;

/**
 * Provides an interface for the SDC Library component manager.
 */
interface ComponentManagerInterface {

  /**
   * A sorted list of all components.
   */
  public function getComponents(): ?array;

  /**
   * Return a render array of all the listed components.
   */
  public function renderComponentList(): array;

  /**
   * Return the full component display of component and form.
   */
  public function renderComponentForm(): array;

  /**
   * Return component render array.
   */
  public function renderComponent(): array;

  /**
   * Get a component instance.
   */
  public function getComponentInstance(string $sdc_plugin): Component;

  /**
   * Gets the arguments.
   */
  public function getArguments(): array;

  /**
   * Generates a render array of the component with the its slots and props.
   */
  public function generateRenderArray(Component $component, array $context): array;

  /**
   * Returns the name of the module or theme that contains the component.
   */
  public function findExtensionName(string $path): ?string;

  /**
   * Called to order component items by their module/theme extension.
   */
  public function sortByComponentName(array $a, array $b): int;

  /**
   * Finds the plugin ID from the sdc component file name.
   *
   * The sdc component file should be in a component directory.
   */
  public function findComponentFile(string $filename): ?string;

  /**
   * List site components grouped by module / theme, then sorted alphabetically.
   */
  public function getComponentsTree(): ?array;

  /**
   * Get the default component props from its component.yml.
   *
   * This gets us the defaults needed to display the component on initial page
   * load.
   */
  public function getPropsFromComponentMetadata(array $schema_properties): ?array;

  /**
   * Get the default component slots from its component.yml.
   */
  public function getSlotsFromComponentMetadata(array $schema_properties): ?array;

  /**
   * Get the component props from user-entered POST data.
   *
   * This is where the user has updated default values as they see fit.
   */
  public function getPropsFromPostedForm(Component $component, array $posted_data): ?array;

  /**
   * Get the component slots from user-entered POST data.
   *
   * This is where the user has updated default values as they see fit.
   */
  public function getSlotsFromPostedForm(Component $component, array $posted_data): ?array;

  /**
   * Return a given property's value.
   */
  public function getPropertyValue(string $type, mixed $property_value): mixed;

}
