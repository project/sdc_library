<?php

namespace Drupal\sdc_library\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Theme\ComponentPluginManager;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\sdc_library\ComponentManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides listing of all components.
 */
class BrowseLibraryController extends ControllerBase {

  /**
   * Controller constructor.
   */
  public function __construct(
    protected $formBuilder,
    private ComponentPluginManager $pluginManager,
    protected ThemeManagerInterface $themeManager,
    protected ComponentManagerInterface $componentManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('form_builder'),
      $container->get('plugin.manager.sdc'),
      $container->get('theme.manager'),
      $container->get('sdc_library.component_manager'),
    );
  }

  /**
   * List all components.
   */
  public function list(Request $request): array {
    $build = $this->componentManager->renderComponentList();
    return $build;
  }

}
