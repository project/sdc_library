<?php

namespace Drupal\sdc_library\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\sdc_library\ComponentManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handles the route to render a given component.
 */
class ComponentDisplayController extends ControllerBase {

  /**
   * Controller constructor.
   */
  public function __construct(
    protected ComponentManagerInterface $componentManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('sdc_library.component_manager'),
    );
  }

  /**
   * Call the component rendering method from our Component Manager service.
   */
  public function render(): array {
    $build = $this->componentManager->renderComponent();
    return $build;
  }

}
