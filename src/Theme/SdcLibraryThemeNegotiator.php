<?php

namespace Drupal\sdc_library\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Allow user to choose theme for component rendering.
 */
class SdcLibraryThemeNegotiator implements ThemeNegotiatorInterface {

  /**
   * Construct the SdcLibraryThemeNegotiator object.
   */
  public function __construct(
    private RequestStack $requestStack,
    private ThemeHandlerInterface $themeHandler,
    protected ConfigFactoryInterface $configFactory,
    protected CurrentRouteMatch $routeMatch,
    protected ThemeExtensionList $themeExtensionList,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match): bool {
    $route_name = $route_match->getRouteName();
    if (!$route_name) {
      return FALSE;
    }

    $request = $this->requestStack->getCurrentRequest();
    // Route parameter {chosen_theme} gives the URL-defined theme.
    $theme = $request->get('chosen_theme');

    // Check if a theme is defined in a POST request. If it is, use it.
    if ($request->request->get('_sdcChosentheme')) {
      $theme = (string) $request->request->get('_sdcChosentheme');
    }

    if (!empty($theme)) {
      $routes = [
        'sdc_library.render',
        'sdc_library.browse',
      ];
      return (in_array($route_name, $routes));
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match): ?string {
    return $this->getTheme();
  }

  /**
   * Gets the theme as requested in the URL parameters.
   *
   * @return string
   *   The theme name.
   */
  private function getTheme(): string {
    // Early return for browse route which uses the default admin theme.
    if ($this->routeMatch->getRouteName() == 'sdc_library.browse') {
      return $this->configFactory->get('system.theme')->get('admin');
    }
    $request = $this->requestStack->getCurrentRequest();
    $theme = $request->get('chosen_theme');

    // Check if we have defined a theme in the POST.
    if ($request->request->get('_sdcChosentheme')) {
      $theme = (string) $request->request->get('_sdcChosentheme');
    }
    if (empty($theme)) {
      return $this->themeHandler->getDefault();
    }
    $theme_list = $this->themeExtensionList->getList();
    $all_themes = array_keys($theme_list);
    if (in_array($theme, $all_themes)) {
      return $theme;
    }
    // Catch-all.
    return $this->themeHandler->getDefault();
  }

}
