<?php

namespace Drupal\sdc_library\Form;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Theme\ComponentPluginManager;
use Drupal\Core\Theme\ThemeInitialization;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Url;
use Drupal\sdc_library\ComponentManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Build component browser form.
 */
class ComponentForm extends FormBase implements FormInterface {

  const TEXTFIELD_SIZE = 100;
  const TEXTFIELD_MAXLENGTH = 512;

  /**
   * SDC List form constructor.
   */
  public function __construct(
    protected EntityTypeManager $entityTypeManager,
    protected ThemeManagerInterface $themeManager,
    protected ThemeInitialization $themeInitialization,
    protected ThemeExtensionList $themeExtensionList,
    protected $requestStack,
    protected $renderer,
    protected string $sdcIframeWrapperId,
    protected $configFactory,
    public string $sdcWelcomeComponent,
    protected ComponentManagerInterface $componentManager,
    protected ComponentPluginManager $componentPluginManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('theme.manager'),
      $container->get('theme.initialization'),
      $container->get('extension.list.theme'),
      $container->get('request_stack'),
      $container->get('renderer'),
      // We remove the "#" because it is not included in ajax wrapper name.
      $sdc_library_iframe_wrapper_id = (string) str_replace('#', '', $container->getParameter('sdc_library.iframe_wrapper_id')),
      $container->get('config.factory'),
      $welcome_component = (string) $container->getParameter('sdc_library.welcome_component'),
      $container->get('sdc_library.component_manager'),
      $container->get('plugin.manager.sdc'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sdc_library_component';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Instantiate some useful variables.
    $themes = $this->themeExtensionList->reset()->getAllAvailableInfo();
    $request = $this->requestStack->getCurrentRequest();

    // Instantiate variables to be used in form.
    $sdcFileName = '';
    $sdcPluginId = '';

    // Grab defaults and then allow route parameters to override them.
    // Grab the default component defined in sdc_library.services.yml.
    $sdcPluginId = 'sdc_library:' . $this->sdcWelcomeComponent;
    // Use the default admin theme as a starting point.
    $chosenTheme = $this->configFactory->get('system.theme')->get('admin');
    if (!empty($request->get('component_plugin')) && !empty($request->get('chosen_theme'))) {
      $sdcPluginId = $request->get('component_plugin');
      $chosenTheme = $request->get('chosen_theme');
    }

    $form['#action'] = '/sdc_library/component/' . $sdcPluginId . '/' . $chosenTheme;
    $form['#attributes'] = [
      'target' => 'sdc-library-iframe',
    ];
    $form['#tree'] = TRUE;

    // Component name is easier to grok than sdcPuginId.
    $component_name = $sdcPluginId;
    // Load component yml to get properties.
    $component = $this->componentPluginManager->createInstance("$sdcPluginId");

    $options = [];
    foreach ($themes as $key => $theme) {
      $options[$key] = $theme['name'];
    }

    asort($options);

    $form['_sdcChosentheme'] = [
      // We can use $this->t() because FormBase uses StringTranslationTrait.
      '#title' => $this->t('Change theme'),
      '#type' => 'select',
      '#default_value' => $chosenTheme,
      '#options' => $options,
    ];

    $form['_sdcFileName'] = [
      '#type' => 'hidden',
      '#value' => $sdcFileName,
    ];

    $form['_sdcPluginId'] = [
      '#type' => 'hidden',
      '#value' => $sdcPluginId,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update component'),
    ];

    $form['props'][$component_name]['props'] = [
      '#type' => 'markup',
      '#markup' => '<h3>' . $this->t('Props') . '</h3>',
    ];
    foreach ($component->metadata->schema['properties'] as $property_name => $prop_schema) {
      $form['props'][$component_name][$property_name] = [
        '#type' => 'fieldset',
        '#value' => $property_name,
      ];
      if (isset($prop_schema['type'])) {
        // @todo The type (ie. "string|number") defined in yml comes out as two
        // values in an array here, "string object", "number object", so it
        // seems everything is its type plus "object", so we use array_shift().
        $type_we_care_about = array_shift($prop_schema['type']);
        $form['props'][$component_name][$property_name]['type'] = [
          '#type' => 'hidden',
          '#value' => $type_we_care_about,
        ];
        $form['props'][$component_name][$property_name]['type_markup'] = [
          '#type' => 'markup',
          '#markup' => $this->t('<p>Type: :type_we_care_about</p>', [':type_we_care_about' => $type_we_care_about]),
        ];
      }
      // If the component does not define defaults, use fallbacks so we have
      // something to render.
      if (!isset($prop_schema['default'])) {
        $prop_schema['default'] = $this->componentManager::PROPERTY_TYPE_DEFAULTS[$type_we_care_about];
      }

      // Use this module's sdc_library.component_manager service to grab values.
      $default_value = $this->componentManager->getPropertyValue($type_we_care_about, $prop_schema['default']);

      // @todo Get rid of this conditional because supporting examples is too
      // much trouble?
      if (!$default_value && isset($prop_schema['examples'][0])) {
        $default_value = $prop_schema['examples'][0];
      }
      // The form element to use is a bit of a mish mash, set mappings here.
      $type_and_format_mapping = [
        'array' => [
          'type' => 'textarea',
          'format' => 'plain_text',
          'default_value' => json_encode($default_value),
        ],
        'integer' => [
          'type' => 'textfield',
          'format' => 'plain_text',
          'default_value' => $default_value,
        ],
        'number' => [
          'type' => 'textfield',
          'format' => 'plain_text',
          'default_value' => $default_value,
        ],
        'string' => [
          'type' => 'text_format',
          'format' => 'full_html',
          'default_value' => $default_value,
        ],
      ];
      // Populate a simply-named variable with the type we need.
      $type = $type_and_format_mapping[$type_we_care_about]['type'];

      // But wait, what if it's a property with a limited set of enum values?
      // Let's build a select list for this use case.
      if (isset($prop_schema['enum'])) {
        $options = [];
        foreach ($prop_schema['enum'] as $value) {
          $type = 'select';
          $options[$value] = $value;
        }
      }

      // Now we should know enough to build the correct type of form element.
      // These three properties are common to the form element we expect to use.
      $form['props'][$component_name][$property_name]['default'] = [
        '#type' => $type,
        '#title' => $this->t('Value'),
        '#default_value' => $type_and_format_mapping[$type_we_care_about]['default_value'],
      ];
      // Here we check for the type and add additional properties.
      if ($type == 'select') {
        $form['props'][$component_name][$property_name]['default']['#options'] = $options;
      }
      else {
        $form['props'][$component_name][$property_name]['default']['#format'] = $type_and_format_mapping[$type_we_care_about]['format'];
        // @todo what is a useful max length?
        $form['props'][$component_name][$property_name]['default']['#maxlength'] = self::TEXTFIELD_MAXLENGTH;
        $form['props'][$component_name][$property_name]['default']['#size'] = self::TEXTFIELD_SIZE;
      }
      // Some components have "examples". We may want to scrap support for them.
      // For now we just display them as markup.
      if (isset($prop_schema['examples'])) {
        foreach ($prop_schema['examples'] as $key => $value) {
          $form['props'][$component_name][$property_name]['examples'][$key] = [
            '#type' => 'markup',
            '#markup' => '<p>' . $this->t('Example') . ' ' . $key + 1 . ': ' . $value . '</p>',
          ];
        }
      }
      if (isset($prop_schema['title'])) {
        $form['props'][$component_name][$property_name]['title'] = [
          '#type' => 'markup',
          '#markup' => $this->t('<p>Title: :title</p>', [':title' => $prop_schema['title']]),
        ];
      }
      if (isset($prop_schema['description'])) {
        $form['props'][$component_name][$property_name]['description'] = [
          '#type' => 'markup',
          '#markup' => $this->t('<p>Description: :description</p>', [':description' => $prop_schema['description']]),
        ];
      }
    }

    // Based on the schema, it looks like slots only have:
    // Title (string)
    // Description (string)
    // Examples (array)
    if (!empty($component->metadata->slots)) {
      // If there are no slots do not build any form output.
      $form['slots'][$component_name]['slots'] = [
        '#type' => 'markup',
        '#markup' => '<h3>Slots</h3>',
      ];
      foreach ($component->metadata->slots as $slot => $info) {
        $form['slots'][$component_name][$slot] = [
          '#type' => 'fieldset',
        ];
        $form['slots'][$component_name][$slot]['data'] = [
          '#type' => 'markup',
          '#markup' => '<h4>' . $slot . '</h4>',
        ];
        if (
          isset($info['title'])
          && isset($info['description'])
        ) {
          $form['slots'][$component_name][$slot]['more_info'] = [
            '#type' => 'markup',
            '#markup' => '<ul><li>' . $this->t('Title: :title', [':title' => $info['title']]) . '</li><li>' . $this->t('Description: :description', [':description' => $info['description']]) . '</li></ul>',
          ];
        }
        // Looks like slots use "examples".
        if (isset($info['examples']) && is_array($info['examples'])) {
          foreach ($info['examples'] as $key => $value) {
            $form['slots'][$component_name][$slot][$key] = [
              '#type' => 'text_format',
              '#format' => 'full_html',
              '#title' => $this->t('Editable value'),
              '#default_value' => $value,
              '#maxlength' => self::TEXTFIELD_MAXLENGTH,
              '#size' => self::TEXTFIELD_SIZE,
            ];
          }
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $sdc_plugin = $form_state->getValue('_sdcPluginId');
    $chosen_theme = $form_state->getValue('_sdcChosentheme');
    $url = Url::fromUserInput('/sdc_library/component/' . $sdc_plugin . '/' . $chosen_theme);
    $form_state->setRedirectUrl($url);
  }

}
