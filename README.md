# SDC Library

SDC (Single Directory Component) Library provides a component library for single
directory components.

The idea is to list all of your site's SDC components (using Drupal*) so that
the user can browse them and see how they look on a theme by theme basis.
Developers can update component markup, styles, and javascript and see the
results right away without any cache clearing. Just make changes to the
component's css / js / twig and click the "Update component" button.

Below the component display is a form with the component properties' default
values. These form values can be edited so that you can enter your own text and
values to display in the component once you click "Update component".

A fairly crucial discovery during development of this module is that it does not
really work without default values for component properties. (See
welcome.component.yml.)

```
  blurb:
    type: string
    title: Blurb
    description: An optional blurb.
    default: "This is my default blurb text."
```

Having defaults allows the component to render with some semblance of how it's
supposed to display. If a component does not have defaults in its `[COMPONENT_NAME].component.yml`,
then fallback text is displayed, pointing this out. (To confuse matters, in
the component schema slots use "examples" while properties use "default", and
some properties may use "examples" instead of "default".)

The component library is available at `/sdc_library` and defaults to display the
Welcome component defined by this module.

Make sure the "Use the SDC Library" permission is granted to the necessary user
roles for your site.

* This module gratefully borrows from the [cl_server module](https://www.drupal.org/project/cl_server)
that was built to use third-party component libraries like Storybook. The CL
Server module has been deprecated and replaced with the [Storybook](https://www.drupal.org/project/storybook)
module.
